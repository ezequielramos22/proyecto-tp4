﻿unit uSistemasLineales;

{$mode objfpc}{$H+} {$M+} //directivas para trabajar con objet
interface

uses
  SysUtils, dialogs, Math;

type

  tVector = array of double;
  tMatriz = array of tVector;

  tCoef = array of array of double;  // Probando




TSistema=class
      private
          matriz:tMatriz;
          independiente:Tvector;
          fc: integer;   // Atributo opcional
      public
          constructor crear(mtz:tMatriz);
          Destructor destruir();
          //Metodos para el acceso a atributos
          procedure setMatriz(mtz:tMatriz);
          function getMatriz():tMatriz;
          
          procedure setIndependiente(ind: tVector);
          function getIndependiente():Tvector;
          procedure mostrarIndependiente();
          function matrizAmpliada(mtz: tMatriz; vInd: tVector): tMatriz;
          procedure mostrarMatrizAmpliada();

          Function NormaV1( v:tVector):double;
          Procedure NormaV2();
          function NormaVinf(vector:tVector):double;
          Procedure NormaM1();
          Procedure NormaMinf();
          Procedure Gauss(mtzA: tMatriz);
          Procedure Gauss_Jordan();

          procedure setFc(p_fc: integer);
          function getFc():integer;
          procedure mostrar(); // Probando

          Procedure crout ();
          function sust_Progresiva(mtz:tMatriz):Tvector;
          function sust_Regresiva(mtz:tMatriz):Tvector;
          Procedure cholesky_L(mtzA:tMatriz; mtzB:tVector; Var X:tVector);
          Procedure cholesky_U(mtzA:tMatriz; mtzB:tVector; Var X:tVector);
       end;

       Implementation

         //Inicializa los Atributos
         constructor TSistema.crear(mtz:tMatriz); //todo ok
         begin
              setFc(Length(mtz)-1); //settea fc con la cantidad de filas de la matriz q viene por parametro.
              setMatriz(mtz);  //va a inicializar el atributo matriz con los valores de la matriz creada en la interfaz
         end;
         Destructor TSistema.destruir();
          begin
               inherited Destroy;
          end;

          procedure TSistema.setFC(p_fc: integer);
          begin
            self.fc := p_fc;
          end;
          function TSistema.getFc(): integer;
         begin
              getFc:=FC;
         end;
         procedure Tsistema.setMatriz(mtz:tMatriz); 
         var fila,columna: byte;
         begin
                setLength(matriz, Length(mtz),Length(mtz)); // Setteo filas de TMatriz
                for fila := 0 to fc do
                    for columna := 0 to fc do
                        matriz[fila][columna]:= mtz[fila][columna];
         end;

         function TSistema.getMatriz(): tMatriz;
         begin
              getMatriz:=matriz;
         end;

         procedure Tsistema.setIndependiente(ind: Tvector);
         var i:byte;
         begin
              for i := 0 to high (ind) do
              begin
                  independiente[i]:=ind[i];
              end;
         end;
        function Tsistema.getIndependiente():tVector;
        begin
            getIndependiente:=independiente;
        end;

        procedure TSistema.mostrar(); //si funciona.
        var
          i, j: integer;
        begin
          for i := 0 to fc do begin
            for j := 0 to fc do begin

              write(matriz[i][j]:2:2, ' ');
            end;
            writeln('');
          end;
        end;

        function Tsistema.NormaV1(v: tVector):double;
        var
         i:integer;
         s:double;
         begin
         s:=0;
         for i:=Low(v) to High(v) do 
             s:=s+abs(v[i]);
            
         NormaV1 :=  s;    
        end;
        
        Procedure Tsistema.NormaV2();
        begin

        end;
       function Tsistema.NormaVinf(vector:tVector):double;  // debe ser funcion
          var
            dMayor,dAux:double;
            i:integer;
        begin
             if vector[0] < 0 then
                 dMayor:=vector[0]*(-1)
             else
                 dMayor:=vector[0];
             for i:=1 to high(vector) do
                 begin
                   if vector[i]< 0 then
                       dAux:= vector[i] *(-1)
                   else
                       dAux:=vector[i];
                   if dMayor < dAux then
                      dMayor:=dAux;
                 end;

             result :=dMayor;

        end;               
        
        Procedure Tsistema.NormaM1();
        begin

        end;
        Procedure Tsistema.NormaMinf();
        begin

        end;
        Procedure Tsistema.Gauss(mtzA: tMatriz);
        var
	 	L,U:tMatriz;
	 	i,j:integer;

			
			procedure multiplica(VAR x:tMatriz; y,z:tMatriz);
			  //multiplica Y*Z en este orden y lo guarda en X
			  var
				i,j,k:integer;
			  begin
				for i:=0 to fc do
				  for j:=0 to fc do begin
					x[i][j]:=0;
					for k:=0 to fc do x[i][j]:=x[i][j]+y[i][k]*z[k][j]
				  end
			  end;
			  
			procedure resta(VAR x:tMatriz; y,z:tMatriz);
			  var
				i,j:integer;
			  begin
				for i:=0 to fc do
				  for j:=0 to fc do
					x[i][j]:=y[i][j]-z[i][j]
			  end;
			  
			procedure factoriza(VAR x,y:tMatriz);
			  var
				i,paso:integer;
				aux,Vu,Vl:tMatriz;
			  begin
				setLength(Vl,Length(mtzA),Length(mtzA));
				setLength(Vu,Length(mtzA),Length(mtzA));
                                setLength(aux,Length(mtzA),Length(mtzA));
				for paso:=0 to fc do begin
				  for i:=0 to fc do begin
					y[paso][i]:=mtzA[paso][i];
					Vu[1][i]:=y[paso][i];
					x[i][paso]:=mtzA[i][paso]/mtzA[paso][paso];
					Vl[i][1]:=x[i][paso]
				  end;
				  multiplica(aux,Vl,Vu);
				  resta(matriz,matriz,aux)
				end
			  end;
			  

				  
		begin
            setLength(L,Length(mtzA),Length(mtzA));
            setLength(U,Length(mtzA),Length(mtzA));
            factoriza(L,U);
			//Muestra
			for i:=0 to fc do begin
				for j:=0 to fc do begin
					write(L[i][j],' ');
				end;
				writeln();
			end;
			writeln();
			writeln();
			writeln();
			for i:=0 to fc do begin
				for j:=0 to fc do begin
					write(U[i][j],' ');
				end;
				writeln();
			end;
			//Fin muestra
        end;
        Procedure Tsistema.Gauss_Jordan();
        begin

        end;

        Procedure Tsistema.crout();
        begin

        end;
        
         //Este metodo agrega una nueva fila a una matriz....
        function addRow(mtzA:tMatriz; mtzB:tVector):tMatriz;
        var
          c:byte; //c:Columna
        begin
            for c:=1 to High(mtzA) do //Recorro las columnas
            begin
                mtzA[High(mtzA)][c] := mtzB[c];  //copia los elementos del vector mtzB 
             end;                                //en la ultima fila de la matriz mtzA
            addRow := mtzA; //retorno la nueva matriz Ampliada
        end;

        // probado. si funciona.
        function tSistema.sust_Progresiva(mtz:tMatriz):Tvector; //cuando nos qda una matriz triangular inferior
        var x:tVector; var fila,columna:integer; acumulador:double; 
        begin //tengo a mtz como la matriz escalonada pro el vector indendiente esta en la ultima fila.
            setLength(x,high(mtz));
            x[0]:=mtz[High(mtz)][0]; // como es una triang unferior unitaria. el primer elemento es 1 siempre.
            for fila:=1 to high(mtz) do
            begin
                 acumulador:=0;
                 for columna:=0 to fila-1 do
                 begin
                      acumulador:=acumulador+ mtz[fila][columna]*x[columna]; //suma los coeficientes con los x ya obtenidos
                 end;
                 acumulador:=(-acumulador)+mtz[High(mtz)][fila]; //pasa de termino y suma con el independiente.
                 x[fila]:=acumulador;  //asigna 
            end;
            result := x;
        end;
//funciona. pro la matriz q viene por parametro tiene q ser cuadrada. ojo.
        function tSistema.sust_Regresiva(mtz:tMatriz):Tvector; //cuando nos qda una matriz triangular superior
        var x:tVector; var fila,columna:integer; acumulador:double;
        begin //tengo a mtz como la matriz escalonada pro el vector indendiente esta en la ultima columna.
            setLength(x,high(mtz));
            x[High(mtz)-1]:=mtz[High(mtz)-1][High(mtz)] / mtz[High(mtz)-1][High(mtz)-1]; // como es una triang sup el ultimo elemento es el x[ultimo]
            //hasta aca funciona bien jajaj hay problemas con los indices y/o logica.
            for fila:= high(mtz)-2 downto 0 do
            begin
                 acumulador:=0;
                 for columna:= High(mtz)-1 downto fila+1 do
                 begin
                      acumulador:=acumulador+ mtz[fila][columna]*x[columna]; //suma los coeficientes con los x ya obtenidos
                 end;
                 acumulador:=(-acumulador)+mtz[fila][High(mtz)]; //pasa de termino y suma con el independiente.
                 acumulador:=acumulador/mtz[fila][fila];
                 x[fila]:=acumulador;  //asigna 
            end;
            result := x;
        end;

        //calculo utilizando cholesky Parte baja
        Procedure Tsistema.cholesky_L(mtzA:tMatriz; mtzB:tVector; Var X:tVector); 
        var
          L: tMatriz; k,i,j:byte; sumatoria:double;
        begin
            setLength(L,Length(mtzA)+1,Length(mtzA)); //Crea el tamaño de la matriz L (N+1xN)
            setLength(X,Length(mtzB));               //Crea el tamaño de la matriz X (Nx1)
            L:=addRow(mtzA,mtzB);                   //Matriz ampliada pero [A/b] Chequear Algoritmo
            for k:=1 to High(mtzA) do               //del profe para comprender XD
            begin

                //Esta parte toma la sumatoria del algorimo de cholesky_L
                sumatoria:=0;
                for j:=1 to (k-1) do
                begin
                    sumatoria:=sumatoria + power(L[k][j],2);
                end;
                //Que esta dentro de la raiz que determina el elemento L[K;k]
            
                L[k][k]:=sqrt( mtzA[k][k]-sumatoria );

                for i:=(k+1) to (High(mtzA)+1) do
                begin
                    
                    //Aca determino otra simatoria
                    sumatoria:=0;
                    for j:=1 to (k-1) do
                    begin
                        sumatoria:=sumatoria + ( L[i][j]*L[k][j] );
                    end;
                    //ya para determinar el elemento L[i;k]

                    L[i][k]:= (mtzA[i][k] - sumatoria)/L[k][k];  
                end;
            end;
            X:=sust_Progresiva(L);
        end;

        //Agrega 1 Columnas a una matriz
        function addColumns(mtzA:tMatriz; mtzB:tVector): tMatriz;
        var
          f:byte; //c:Columna
        begin
            for f:=1 to High(mtzA) do //Recorro las filas
            begin
                mtzA[f][High(mtzA)] := mtzB[f];  //copia los elementos del vector mtzB 
             end;                                //en la ultima columna de la matriz mtzA
            addColumns := mtzA; //retorno la nueva matriz Ampliada
        end;

        //Calculo Utilizando Cholesky Parte Alta
        procedure tSistema.cholesky_U(mtzA:tMatriz; mtzB:tVector; Var X:tVector);
        var
          U: tMatriz; k,i,j:byte; sumatoria:double;
        begin
            setLength(U,Length(mtzA),Length(mtzA)+1);  //Crea el tamaño de la matriz U (NxN+1)
            setLength(X,Length(mtzB));                //Crea el tamaño de la matriz X (Nx1) 
            U:=addColumns(mtzA,mtzB);                //Matriz Ampliada [A|b]
            for k:=1 to High(mtzA) do
            begin

                //Esta parte toma la sumatoria del algorimo de cholesky_U
                sumatoria:=0;
                for i:=1 to (k-1) do
                begin
                    sumatoria:=sumatoria + power(U[i][k],2);
                end;
                //Que esta dentro de la raiz que determina el elemento U[K;k]
            
                U[k][k]:=sqrt( mtzA[k][k]-sumatoria );

                for j:=(k+1) to (High(mtzA)+1) do
                begin
                    
                    //Aca determino otra simatoria
                    sumatoria:=0;
                    for i:=1 to (k-1) do
                    begin
                        sumatoria:=sumatoria + ( U[i][k]*U[i][j] );
                    end;
                    //ya para determinar el elemento U[k;j]

                    U[k][j]:= ( mtzA[k][j] - sumatoria)/U[k][k];  
                end;
            end;
            //Faltaria implementarlo
            //X:=sust_Regresiva(U);
        end;

        procedure tSistema.mostrarIndependiente();
          var
            i: integer;
          begin
            for i := 0 to high(getIndependiente()) do begin
              write(Independiente[i]:2:2, ' ');
            end;
          end;

        // Devuelve una nueva TMatriz
        function tSistema.matrizAmpliada(mtz: tMatriz; vInd: tVector): tMatriz;
          var
            matrizAux: tMatriz;
            i, j: integer;
          begin
            setLength(matrizAux, Length(mtz), Length(mtz) + 1); // Agrega la columna de coeficientes
            for i := 0 to High(mtz) do begin // Copia datos de matriz original
              for j := 0 to High(mtz) do begin
                matrizAux[i][j] := mtz[i][j];
              end;
            end;
            for j := 0 to High(mtz) do begin // Asigna datos de vector independiente en la nueva columna
              matrizAux[j][High(mtz) + 1] := vInd[j];
            end;
            result := matrizAux;
          end;

        procedure tSistema.mostrarMatrizAmpliada();
          var
            matrizA: tMatriz;
            i, j: integer;
          begin
            matrizA := matrizAmpliada(getMatriz(), getIndependiente());
            for i := 0 to High(matrizA) do begin
              for j := 0 to Length(matrizA) do begin
                write(matrizA[i][j]:2:2, ' ');
              end;
              writeln('');
            end;
          end;

begin

end.
