program Test;
uses uSistemasLineales;

var matriz:Tsistema; mtz:Tmatriz; orden:integer;
begin
	//mtz es el que sera pasado por parametro y proviene de la interfaz
	//en este caso la inicializaremos acá para realizar pruebas.

	orden:=3;
	setLength(mtz,orden,orden); //le damos un tamanio de 3x3. 
	mtz[0,0]:=6;  mtz[0,1]:=-2;  mtz[0,2]:=2;
	mtz[1,0]:=12; mtz[1,1]:=-8;  mtz[1,2]:=6;
	mtz[2,0]:=3;  mtz[2,1]:=-13; mtz[2,2]:=9;

	matriz:=Tsistema.crear(mtz);
	matriz.mostrar();
	matriz.Gauss(mtz);
	readln;

end.
