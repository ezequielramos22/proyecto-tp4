program tp4;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, Sistema
  { you can add units after this };
var
  s:Sistemas;
  n:integer;
begin
     n:=4;
     s:=Sistemas.Crear(n);
     s.NormaM1();
     s.NormaMinf();
     s.NormaV1();
     s.NormaV2();
     s.NormaVinf();
     s.Gauss();
     s.Gauss_Jordan();
     readln;
end.


