unit Sistema;

{$mode objfpc}{$H+}

interface
         Uses
         Classes, SysUtils;
         type
         Sistemas=class
         private

           M_Coef:array [1..6,1..6] of double;
           b:array [1..6] of double;
           orden:integer;
           Function Getorden():integer;
           Procedure Setorden(n:integer);
           Procedure SetMatrizCoef();
           Function GetMatrizCoef(i:integer;j:integer):double;
           Procedure SetVectorIndep();
         public
           Constructor Crear(n:integer);
           Destructor Listo();
           Procedure NormaV1();
           Procedure NormaV2();
           Procedure NormaVinf();
           Procedure NormaM1();
           Procedure NormaMinf();
           Procedure Gauss();
           Procedure Gauss_Jordan();
         end;

implementation

Function Sistemas.Getorden():integer;
begin
     Getorden:=orden;
end;
Procedure Sistemas.Setorden(n:integer);
     begin
     orden:=n;
     end;
           Constructor Sistemas.Crear(n:integer);
           begin
                          inherited Create;
                          Setorden(n);
                          SetMatrizCoef();
                          SetVectorIndep();
           end;
Destructor Sistemas.Listo();
begin
     inherited Destroy;
end;
Procedure Sistemas.NormaV1();
var
s:double;
i:integer;
x:array [1..6] of double;
begin
     for i:=1 to Getorden do
         begin
         writeln('Ingrese elemento del vector',i);
         readln(x[i]);
         end;
     s:=0;
     for i:=1 to Getorden do
          s:=s+abs(x[i]);
     writeln ('Norma 1 es:',s);
end;
Procedure Sistemas.NormaV2();
var
i:integer;
s:double;
x:array [1..6] of double;
begin
     for i:=1 to Getorden do
         begin
         writeln('Ingrese elemento del vector',i);
         readln(x[i]);
         end;
     s:=0;
     for i:=1 to Getorden do
          s:=s+sqr(x[i]);
     s:=sqrt(s);
     writeln ('Norma 2 es:',s);
end;
Procedure Sistemas.NormaVinf();
var
i:integer;
s:double;
x:array [1..6] of double;
begin
      for i:=1 to Getorden do
         begin
         writeln('Ingrese elemento del vector',i);
         readln(x[i]);
         end;
      s:=x[1];
      for i:=2 to Getorden do
         if (abs(s)<abs(x[i])) then
            s:=x[i];
      writeln('Norma Infinita es:',s);
end;
Procedure Sistemas.NormaMinf();
var
i,j:integer;
norma,s:double;
begin
     norma:=0;
   for i:=1 to getorden do
      begin
           s:=0;
           for j:=1 to getorden do
               begin
                    s:=s+GetMatrizCoef(i,j);
               end;
           if (s>norma) then
              norma:=s;
      end;
   writeln('Norma Infinita de la Matriz es',norma);
end;
Procedure Sistemas.NormaM1();
var
norma,s:double;
i,j:integer;
begin
     norma:=0;
   for j:=1 to getorden do
      begin
           s:=0;
           for i:=1 to getorden do
               begin
                    s:=s+GetMatrizCoef(i,j);
               end;
           if (s>norma) then
              norma:=s;
      end;
   writeln('Norma 1 de la Matriz es',norma);
end;

Procedure Sistemas.Gauss();
var
g:array[1..7,1..7] of double;
aux,m:double;
k,n,i,j:integer;
x:array[1..6] of double;
begin
n:=Getorden();
for i:=1 to n do
   begin
        for j:=1 to n do
             begin
                 g[i][j]:=M_Coef[i][j];
             end;
   end;
   for i:=1 to n do
        g[i][n+1]:=b[i];
   for k:=1 to n-1 do
      begin
           writeln('Iteracion k',k);
           for i:=k+1 to n do
             begin
                m:=g[i][k]/g[k][k];
                writeln('Multiplicador para fila',i,':',m);
                g[i][k]:=0;
                for j:=k+1 to n+1 do
                begin
                     g[i][j]:=g[i][j]-(m*g[k][j]);
                end;
            end;
      end;
  for i:=n downto 1 do
     begin
         j:=n;
         aux:=0;
         while (i<>j) do
             begin
                 aux:=aux+g[i][j]*x[j];
                 j:=j-1;
            end;
        x[i]:=(g[i][n+1]-aux)/g[i][j];
     end;
  for i:=1 to n do
    writeln('Vector solucion, posicion',i,x[i]);
end;
Procedure Sistemas.Gauss_Jordan();
var
g:array[1..7,1..7] of double;
m:double;
k,n,i,j:integer;
x:array[1..6] of double;
begin
   n:=Getorden();
   for i:=1 to n do
      begin
         for j:=1 to n do
             begin
                   g[i][j]:=M_Coef[i][j];
             end;
      end;
    for i:=1 to n do
          g[i][n+1]:=b[i];
    for k:=1 to n do
        begin
             writeln('Iteracion k',k);
             for i:=1 to n do
                 if (i<>k) then
                    begin
                         m:=g[i][k]/g[k][k];
                         writeln('Multiplicador para fila',i,':',m);
                         for j:=k to n+1 do
                             g[i][j]:=g[i][j]-(m*g[k][j]);
                    end;
        end;
    for i:=1 to n do
        begin
             x[i]:=g[i][n+1]/g[i][i];
             writeln('Vector solucion',i,x[i]);
        end;
end;
Procedure Sistemas.SetMatrizCoef();
var
  i,j:integer;
begin
     for i:=1 to Getorden() do
         begin
         for j:=1 to Getorden() do
             begin
                 writeln('Ingrese Fila',i,'Columna',j);
                 readln(M_Coef[i][j]);
             end;
         end;
end;
Function Sistemas.GetMatrizCoef(i:integer;j:integer):double;
begin
     GetMatrizCoef:=M_Coef[i][j];
end;

Procedure Sistemas.SetVectorIndep();
var
i:integer;
begin
     for i:=1 to Getorden() do
         begin
         writeln('elemento',i,'del vector independiente');
         readln(b[i]);
         end;
end;
end.
