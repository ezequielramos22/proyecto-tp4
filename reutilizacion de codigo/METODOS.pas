program Mindirecto;


uses
crt;
type

  matriz= array [1..8,1..8] of Double;
  vector=array[1..8] of Double;

  Indirectos=class
  private
     matriz1:matriz;
     vec:vector;
     vecb:vector;
     epsilon:Double;
     cantIter:integer;
     tama:integer;
  public
      constructor crear(t:Integer;ep:Double;ite:Integer);
      procedure cargarM ();
      function norma(v1:vector):Double;
      procedure Jacobi(var x:vector);
      procedure GaussSeidel(var x:vector);
      procedure Relajamiento(var x:vector);
      procedure mostrar(v1:vector);
      procedure cargarV(var v1:vector);
      procedure multiplica( vx:vector;var v3:vector);
      procedure modificox(var v:vector;vr:vector) ;
      procedure resta(var v2:vector) ;
      function mayorelemento(v2:vector):integer;
      procedure Set_epsi(Const val:Double);
      procedure mostrarm();
      procedure set_b();
     procedure set_x() ;
      procedure restadosvectores(v1:vector;var v2 :vector);

end;
 constructor Indirectos.crear(t:Integer;ep:Double;ite:Integer);
 begin

   epsilon:=ep;
   cantIter:=ite;
   tama:=t;
 end;


procedure Indirectos.Set_epsi(Const val:Double);
 begin
   if epsilon<>val then
      epsilon:=val;
 end;

procedure Indirectos.multiplica( vx:vector;var v3:vector);
var
 i,k:integer;
 s:Double;
begin
    s:=0;
    for i:=1 to tama do
     begin
      for k:=1 to tama do
       begin

         s:=s+matriz1[i,k]*vx[k];
       end;
      v3[i]:=s;
      s:=0; {este vector lo devuelve por parametro}
     end;

end;
procedure Indirectos.resta(var v2:vector) ;
var
 i:integer;
 aux:Double;
begin

    for i:=1 to tama do
     begin
      aux:=v2[i];
      v2[i]:=vecb[i]-aux;
     end;
end;
function positivo(x:Double):Double;

 begin
   if x<0 then
      x:=x*(-1);
 end;

function Indirectos.mayorelemento(v2:vector):integer;
var
 i,k:integer;
 aux:Double;
begin
   k:=1;
   aux:=v2[1];
   for i:=1 to tama do
     if (positivo(aux)>positivo(v2[i])) then
       k:=1
     else
         begin
         aux:=v2[i];
         k:=i;
         end;
 Result:=k;
end;
procedure Indirectos.modificox(var v:vector;vr:vector) ;
var
  pos:integer;
begin
   pos:=mayorelemento(vr);
   Writeln(pos);
   v[pos]:=v[pos]+(vr[pos]/matriz1[pos,pos]);
end;


procedure Indirectos.cargarV(var v1:vector);
var
i:integer;
begin
  for i:=1 to tama do
   begin
    writeln('Ingrese valores de vector');
    readln(v1[i]);
   end;
end;

procedure Indirectos.cargarM();
  var
   i,j,m:integer ;
  begin
    m:=tama*tama;
    Writeln('Ingrese los ',m,' valores : ');
   for i:=1 to tama do
    begin
    for j:=1 to tama do
     begin
      readln(matriz1[i,j]);
      end;
    end;
  end;

  procedure Indirectos.mostrar(v1:vector);
  var
  l:integer ;
   begin
    Writeln('solucion: ');
    for l:=1 to tama do
     begin

      Writeln(v1[l]:2:2) ;
     end;
  end;
 function Indirectos.norma(v1:vector):Double;
  var
   i:integer;
    s:Double;
  begin
    s:=0;
    for i:=1 to tama do
     begin
         s:=s+sqr(v1[i]);
     end;
   s:=sqrt(s);
   result:=s;
  end;
 procedure Indirectos.Jacobi(var x:vector);

   var
    contador,i,j,k:integer;
     s,s1:Double;
     aux,r:vector;
      B:boolean;
  begin
   aux:=vec ;
   B:=false;
   multiplica(aux,r);
   mostrar(r);
   resta(r);
   mostrar(r);
   s1:=norma(r);




   for k:=1 to tama do
       begin
        x[k]:=vecb[k]/matriz1[k,k];
       end;



   contador:=1;
   while (s1>epsilon) and (contador<=cantIter) do
       begin
       B:=true;
         for i:=1 to tama do
           begin
            s:=0;
            for j:=1 to i-1 do
              begin
                s:=s+matriz1[i,j]*aux[j];
              end;
            for j:=i+1 to tama do
              begin
                s:=s+matriz1[i,j]*aux[j];
              end;
            x[i]:=(vecb[i]-s)/matriz1[i,i];
          end;
         aux:=x;
        multiplica(aux,r);
        resta(r);
        s1:=norma(r);
        contador:=contador+1;
      end;
   if (contador=1) then
     begin
         x:=vec;
       end;
  end;


procedure Indirectos.GaussSeidel(var x:vector);

   var
    contador,i,j,k:integer;
     s1,s2,s,n:Double;
     aux,r:vector;
      B:boolean;
  begin
   aux:=vec ;

   multiplica(aux,r);

   resta(r);

   n:=norma(r);




   for k:=1 to tama do
       begin
        x[k]:=vecb[k]/matriz1[k,k];
       end;
   mostrar(x);
    contador:=1;
   while (n>epsilon) and (contador<=cantIter)  do
       begin
        for i:=1 to tama do
          begin
           s1:=0;
           for j:=i+1 to tama do
             begin
              s1:=s1+(matriz1[i,j]*aux[j]);
             end;
           Writeln('s1:',s1);
           s2:=0;
           for j:=1 to (i-1) do
             begin
              s2:=s2+(matriz1[i,j]*aux[j]);
             end;
           writeln('s2: ',s2) ;

           x[i]:=(vecb[i]-s1-s2)/matriz1[i,i];
           writeln(x[i],i);
          end;
           aux:=x;
        multiplica(aux,r);
        resta(r);
        n:=norma(r);
        contador:=contador+1;

       end;
   if (contador=1) then
     begin
         x:=vec;
       end;

  end;

procedure Indirectos.Relajamiento(var x:vector);
   var
       cont,i,j,k:integer;
        s1,s2,s,n:Double;
        aux,r:vector;
         B:boolean;
     begin
      x:=vec ;

      multiplica(x,r);

      resta(r);
        cont:=0;
      n:=norma(r);
      while (n>epsilon) and (cont<cantIter) do
         begin
          mostrar(r);
         modificox(x,r) ;
           multiplica(x,r);
           resta(r);
           mostrar(r);
           n:=norma(r);
           cont:=cont+1;
          end;



end;
procedure Indirectos.restadosvectores(v1:vector;var v2 :vector);
var
   i:integer;

begin
 for i:=1 to tama do
   v2[i]:=v1[i]-v2[i];
end;

procedure Indirectos.mostrarm();
 var
   i,j:integer;
   begin
   for i:=1 to tama do
     for j:=1 to tama do
     begin
      writeln(matriz1[i,j]:2:2);
      end;
   end;
procedure Indirectos.set_b();
var
k:integer;

  begin
 for k:=1 to tama do
  begin
   writeln('Ingrese el elementos',k,' del vector b : ');
   readln(vecb[k]);
  end;
end;

procedure Indirectos.set_x();
var
k:integer;

begin
writeln('Ingrese los elementos del vector a aproximar :');
 for k:=1 to tama do
     readln(vec[k]);
end;


var
  mimetodo:Indirectos;
   m:matriz;

   vx:vector;
   e:Double;
   cont:integer;
   tamano:integer;

begin
    clrscr;
    TextColor(120);
    GotoXY(5,2);
    writeln('********** Sistema Indirectos para Sistemas de Ecuaciones**********');
     TextColor(120);
     GotoXY(1,6);
    Write('Ingrese tamanio de la matriz: ');readln(tamano);
    write('Ingrese el error : ');readln(e);
    write('Ingrese la cantidad de Iteraciones : ');readln(cont);
    mimetodo:=Indirectos.crear(tamano,e,cont);
    mimetodo.cargarM();
    clrscr;
    mimetodo.set_b();
    mimetodo.set_x();
    mimetodo.Relajamiento(vx);
    mimetodo.mostrar(vx);
    readln;
end.
                            